<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sistema de Noticias</title>
    
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:400,700,400italic);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            padding: 10px;
        }

        .welcome {
            color: #242a30;
            width: 300px;
            height: 200px;
            margin: 0 auto;
            text-align:center;
        }

        a, a:visited {
            text-decoration:none;
        }

        h1 {
            font-size: 32px;
            margin: 16px 0 0 0;
        }

        h1, h2 {
            font-weight: 700;
        }

        li{
            margin: 10px 0;
        }

        .request {
            color: #3b73af;
            font-style: italic;
        }

        .table {
            width: 100%;
            border: 1px solid #242a30;
            margin-bottom: 20px;
            border-radius: 3px;
            background: white;
        }

        .table th {
            text-align: left;
            border-bottom: 2px solid #242a30;
            color: #242a30;
            font-weight: 700;
            padding: 10px 15px;
        }

        .table td {
            padding: 10px 15px;
            line-height: 1.42857143;
            border-bottom: 1px solid #e2e7eb;
        }

        .table tr:last-child td {
            border: none;
        }

    </style>
</head>
<body>
    <div class="welcome">
        <h1>WebService para Chat e Emails</h1>
    </div>

    <div >
        <h3>Tabela de Recursos</h3>
        <table class='table'>
            <thead>
                <tr>
                    <th>Status</th>
                    <th>C&oacute;digo</th>
                    <th>Mensagem</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>