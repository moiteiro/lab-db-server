<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::post('user-registration', 'UserController@create');

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    /*Dashboard URLs*/
    Route::get('get-statistics', 'DashboardController@getStatistics');

    /*User URLs*/
    Route::get('user-list', 'UserController@getUserList');

    /*Chat URLs*/
    Route::post('get-user-conversation', 'ChatController@getUserConversationById');
    Route::post('save-chat', 'ChatController@saveUserChat');

    /* Private Messages URLs */
    Route::post('get-private-message-notifications','PrivateMessageController@getUserNotifications');
    Route::post('get-private-messages','PrivateMessageController@getPrivateMessages');
    Route::post('get-private-message','PrivateMessageController@getPrivateMessageById');
    Route::post('get-private-messages-sent','PrivateMessageController@getPrivateMessagesSent');
    Route::post('send-private-message','PrivateMessageController@sendPrivateMessage');

});
