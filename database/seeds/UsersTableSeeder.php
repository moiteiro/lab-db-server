<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userOne = [
        	'name' => 'Moiteiro',
        	'email' => 'bruno.moiteiro@gmail.com',
        	'password' => Hash::make('secret'),
        ];

        User::create($userOne);
    }
}
