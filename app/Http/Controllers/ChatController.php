<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Illuminate\Http\Request;
use Vinkla\Pusher\Facades\Pusher as LaravelPusher;
use DB;

class ChatController extends Controller
{
    public function getUserConversationById(Request $request)
    {
        $userId = $request->input('id');
        $authUserId = $request->user()->id;
        $client_id = $request->user()->token()->client_id;
        
        $query = DB::select('SELECT * FROM `chats` 
            WHERE `client_id` = ? AND 
            `sender_id` IN (?, ?) AND 
            `receiver_id` IN (?, ?) 
            ORDER BY `created_at` ASC',
             [$client_id, $authUserId, $userId, $authUserId, $userId]);

        $authUser = DB::select('SELECT * FROM `users` WHERE id = ? LIMIT 1', [$authUserId]);
        $user = DB::select('SELECT * FROM `users` WHERE id = ? LIMIT 1', [$userId]);

        foreach ($query as $key => $value) {
            if ($value->sender_id == $userId)  {
                $query[$key]['sender'] = $user;
                $query[$key]['receiver'] = $authUser;
            } else {
                $query[$key]['sender'] = $authUser;
                $query[$key]['receiver'] = $user;
            }
        }

        return response(['data' => $query], 200);
    }

    public function saveUserChat(Request $request)
    {
        $attributes = [
            $request->user()->token()->client_id,
            $request->user()->id,
            $request->input('receiver_id'),
            $request->input('chat'),
            1
        ];

        $result = DB::insert('
            INSERT INTO `chats` 
                (`client_id`, `sender_id`, `receiver_id`, `chat`, `read`, `updated_at`, `created_at`)
            VALUES 
                (?, ?, ?, ?, ?, NOW(), NOW())', 
            $attributes);

        $lastId = DB::getPdo()->lastInsertId();

        $finalData = DB::select('SELECT * FROM `chats` WHERE id = ?', [$lastId]);

        LaravelPusher::trigger('chat_channel', 'chat_saved', ['message' => $finalData]);

        return response(['data' => $finalData], 200);
    }
}
