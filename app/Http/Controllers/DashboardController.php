<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $userId = 1;

        $query = DB::select("SELECT 
            (SELECT COUNT(id) FROM users) as users,
            (SELECT COUNT(id) FROM chats WHERE sender_id = {$userId}) AS chat_sent,
            (SELECT COUNT(id) FROM chats WHERE receiver_id = {$userId}) AS chat_received,
            (SELECT COUNT(id) FROM private_messages WHERE sender_id = {$userId}) AS pm_sent,
            (SELECT COUNT(id) FROM private_messages WHERE receiver_id = {$userId}) AS pm_received,
            (SELECT COUNT(id) FROM private_messages WHERE receiver_id = {$userId} AND `read` = 0) AS pm_received_not_read;");
        return $query;
    }

    public function getStatistics(Request $request)
    {
        $userId = $request->user()->id;

        $query = DB::select("SELECT 
            (SELECT COUNT(id) FROM users) as users,
            (SELECT COUNT(id) FROM chats WHERE sender_id = {$userId}) AS chat_sent,
            (SELECT COUNT(id) FROM chats WHERE receiver_id = {$userId}) AS chat_received,
            (SELECT COUNT(id) FROM private_messages WHERE sender_id = {$userId}) AS pm_sent,
            (SELECT COUNT(id) FROM private_messages WHERE receiver_id = {$userId}) AS pm_received,
            (SELECT COUNT(id) FROM private_messages WHERE receiver_id = {$userId} AND `read` = 0) AS pm_received_not_read;");

        return response(['data' => $query], 200);
    }
}
