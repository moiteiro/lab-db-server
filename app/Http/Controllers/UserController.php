<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use DB;

class UserController extends Controller
{
    public function getUserList() 
    {
        $query = DB::select("SELECT * FROM `users` ORDER BY `name` asc");
        return response(['data' => $query], 200);
    }

    public function create(Request $request)
    {
        $attributes = [
            $request->input('name'),
            $request->input('username'),
            Hash::make($request->input('password'))
        ];

        $result = DB::insert('
            INSERT INTO `users` 
                (`name`, `email`, `password`, `updated_at`, `created_at`) 
            VALUES 
                (?, ?, ?, NOW(), NOW())', 
            $attributes);
        
        if ($result) {
            return response(['data' => $result], 200);
        }

        return response(['data' => 'error'], 400);
    }
}
