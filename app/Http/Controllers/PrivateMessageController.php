<?php

namespace App\Http\Controllers;

use App\PrivateMessage;
use Illuminate\Http\Request;
use Vinkla\Pusher\Facades\Pusher as LaravelPusher;
use DB;

class PrivateMessageController extends Controller
{
    public function getUserNotifications(Request $request)
    {
        $client_id = $request->user()->token()->client_id;
        $user_id = $request->user()->id;

        $notifications = DB::select("
            SELECT * FROM `private_messages` 
            WHERE `client_id` = ? AND
            `read` = 0 AND 
            `receiver_id` = ? 
            ORDER BY created_at DESC", 
            [$client_id, $user_id]);
        return response(['data' => $notifications], 200);
    }

    public function getPrivateMessages(Request $request)
    {
        $client_id = $request->user()->token()->client_id;
        $user_id = $request->user()->id;

        $pms = DB::select("
            SELECT * FROM `private_messages` 
            WHERE `client_id` = ? AND
            `receiver_id` = ? 
            ORDER BY `created_at` DESC", 
            [$client_id, $user_id]);
        return response(['data' => $pms], 200);
    }

    public function getPrivateMessageById(Request $request)
    {
        $client_id = $request->user()->token()->client_id;
        $user_id = $request->user()->id;
        $id = $request->input('id');        

        $pm = DB::select('
            SELECT * FROM `private_messages` 
            WHERE `client_id` = ? AND
            `receiver_id` = ? AND
            `id` = ? 
            LIMIT 1', 
            [$client_id, $user_id, $id]);

        if ($pm[0]->read == 0) {
            DB::update('
                UPDATE `private_messages` 
                SET `read` = ?, `updated_at` = NOW() 
                WHERE `id` = ?', 
                [1, $request->input('id')]);
        }
        

        return response(['data' => $pm], 200);
    }

    public function getPrivateMessagesSent(Request $request) 
    {
        $client_id = $request->user()->token()->client_id;
        $user_id = $request->user()->id;
        
        $pms = DB::select("SELECT * FROM `private_messages` 
            WHERE `client_id` = ? AND
            `sender_id` = ? 
            ORDER BY `created_at` DESC", 
            [$client_id, $user_id]);

        return response(['data' => $pms], 200);
    }

    public function sendPrivateMessage(Request $request) 
    {
        $attributes = [
            $request->user()->token()->client_id,
            $request->user()->id,
            $request->input('receiver_id'),
            $request->input('subject'),
            $request->input('message'),
            0,
        ];

        $result = DB::insert('
            INSERT INTO `private_messages` 
                (`client_id`, `sender_id`, `receiver_id`, `subject`, `message`, `read`, `updated_at`, `created_at`)
            VALUES 
                (?, ?, ?, ?, ?, ?, NOW(), NOW())', 
            $attributes);

        $lastId = DB::getPdo()->lastInsertId();

        $finalData = DB::select('SELECT * FROM `private_messages` WHERE id = ?', [$lastId]);
        
        LaravelPusher::trigger('pm_channel', 'pm_saved', ['message' => $finalData]);

        return response(['data' => $finalData], 201);
    }
}